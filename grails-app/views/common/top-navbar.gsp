<nav class="navbar navbar-default navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target="#em-navbar-links">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="${createLink(controller: 'home')}" class="navbar-brand">
                <span class="glyphicon glyphicon-briefcase"></span>
                <strong class="">E</strong><span class="">xpense</span><strong class="">M</strong><span class="">anager</span>
            </a>
        </div>
        <div id="em-navbar-links" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="${createLink(controller: 'home') }">
                        <g:message code="em.home"/>
                    </a>
                </li>
                <li class="">
                    <a href="${createLink(controller: 'home') }">
                        <g:message code="em.expenses"/>
                    </a>
                </li>
                <li class="">
                    <a href="${createLink(controller: 'home') }">
                        <g:message code="em.settings"/>
                    </a>
                </li>
                <li class="">
                    <a href="${createLink(controller: 'home') }">
                        <g:message code="em.contactus"/>
                    </a>
                </li>
                <li class="visible-xs visible-sm">
                    <a href="${createLink(controller: 'home', action: 'loginOrSignUp') }">
                        <span class="glyphicon glyphicon-user"></span>
                        <g:message code="em.loginOrSignUp"/>
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav pull-right hidden-xs hidden-sm">
                <li>
                    <button type="button" class="btn navbar-btn btn-success" onclick="window.location='${createLink(controller: 'home', action: 'loginOrSignUp') }'">
                        <span class="glyphicon glyphicon-user"></span>
                        <g:message code="em.login"/>
                    </button>
                </li>
                <li>
                    <button type="button" class="btn navbar-btn btn-warning" onclick="window.location='${createLink(controller: 'home', action: 'loginOrSignUp') }'">
                        <g:message code="em.signup"/>
                    </button>
                </li>
            </ul>
            
            
            
        </div>
    </div>
</nav>