<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><g:message code="em.homepage"/></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no">
<r:require modules="bootstrap"/>
<link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}" type="text/css">
<r:layoutResources />