<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
    <head>
        <g:include view="/common/common-head.gsp"/>
    </head>
    <body>
        <g:include view="/common/top-navbar.gsp"/>
        <div class="container login-form-wrapper">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <form>
                        <div class="input-group">
                            <span class="input-group-addon glyphicon glyphicon-user"></span>
                            <input type="text" class="form-control" name="username" placeholder="${message(code: 'em.username')}"/>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon glyphicon glyphicon-lock"></span>
                            <input type="text" class="form-control" name="password" placeholder="${message(code: 'em.password')}"/>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon glyphicon glyphicon-lock"></span>
                            <input type="text" class="form-control" name="confirmPassword" placeholder="${message(code: 'em.confirmPassword') }"/>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <button type="submit" class="btn primary-btn">
                                <g:message code=""/>
                            </button>
                        </div>
                        <div class="col-md-3 col-sm-12">
                        </div>
                    </form>
                </div>
                <div class="col-md-6 col-sm-12">
                </div>
            </div>
        </div>
        <g:include view="/common/footer.gsp"/>
        <r:layoutResources />
    </body>
</html>
