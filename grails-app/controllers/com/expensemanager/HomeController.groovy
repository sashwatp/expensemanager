package com.expensemanager

class HomeController {

    def index() {
        render(view: "/common/home")
    }
    
    def loginOrSignUp() {
        render(view: "/common/login-or-sign-up")
    }
}
