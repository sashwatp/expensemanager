package com.expensemanager

class UserProfile {

    String firstName
    String lastName
    
    static belongsTo = [user: User]
    
    static constraints = {
        
    }
}
