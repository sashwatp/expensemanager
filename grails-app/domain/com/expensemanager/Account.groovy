package com.expensemanager

class Account {
	String accountName
	String description
	
    static hasMany = [users : User]
    static belongsTo = User
    
    static constraints = {
        accountName blank:false
    }
}
