package com.expensemanager

class User {
    String username
    String email
    String password
    boolean isActive

    static hasMany = [accounts : Account]
    
    static constraints = {
        username blank:false, unique:true
        email email:true
        password blank:false, password: true
    }
}
