package com.expensemanager

class Expense {
	
    Date expenseDate
	long amount
    
    static hasOne = [category: ExpenseCategory]
    
    static belongsTo = [account: Account]
    
    static constraints = {
		expenseDate blank:false
		amount blank:false, min:0L 
    }
}
