package com.expensemanager

class ExpenseCategory {
	String categoryName
	String description
	
    static hasMany =[expenses: Expense]
    
    static constraints = {
		categoryName blank:false
    }
}
