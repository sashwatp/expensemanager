package com.expensemanager

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(ExpenseCategory)
class ExpenseCategorySpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test valid Expense Category"() {
        def expenseCategory = new ExpenseCategory(expenseCategory: "food")
        
        assert expenseCategory.validate()
    }
}
