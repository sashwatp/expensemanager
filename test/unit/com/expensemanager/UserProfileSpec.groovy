package com.expensemanager

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(UserProfile)
class UserProfileSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test valid user profile"() {
        def user = new User(
                        username: "sashawt",
                        password: "password"
                    )
        
        def userProfile = new UserProfile(firstName: "Sashwat", lastName: "Prakash")
        
        assert userProfile.validate()
    }
}
