package com.expensemanager

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(User)
class UserSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "Test Invalid User with password same as username"() {
        def username = "sashwat"
        def user = new User(username: username, password: username)
        
        assert !user.validate()
        
        assert password == user.errors['password']
    }
}
